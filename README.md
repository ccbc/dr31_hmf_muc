# Analysis of the metastatic urothelial carcinoma DR31 cohort (CPCT-02)

This repository contains R scripts to perform WGS and RNA-seq analysis of the metastatic urothelial carcinoma (mUC) cohort of the CPCT-02 consortium (n = 116 and n = 90, respectively) published by Nakauma-Gonzalez, Rijnders, *et al.* in European Urology (2022): [Molecular characterization reveals genomic and transcriptomic subtypes of metastatic urothelial carcinoma](https://doi.org/10.1016/j.eururo.2022.01.026). This workflow is dependent on pre-processed Hartwig Medical Foundation (HMF) data which are used to generate figures and process data for the manuscript. Access to HMF data is restricted and must be requested under the request number DR-031. WGS data, RNA-seq data and corresponding clinical data are freely available for academic use from the HMF through standardized procedures. Request forms can be found at <https://www.hartwigmedicalfoundation.nl>.

A classifier for mUC is available in the *classifier* folder. Load the classifier:
```         
source("classifier/mUC_classify.R")
load("data/centroids_mUC.RData")
```
Apply the classifier on a RNA count matrix (matrixCounts; rows = gene symbols, columns = sampleId)
```         
mUCsubtype <- classifymUC(x = normalizedCountMatrix, centroids_mUC = centroids_mUC)
```


## Additional required software modules

This workflow (in *R*) was performed on Ubuntu 18.04.5 LTS with the following *R* version:

```         
R version 3.6.3 (2020-02-29) -- "Holding the Windsock"
Copyright (C) 2020 The R Foundation for Statistical Computing
Platform: x86_64-pc-linux-gnu (64-bit)
```

Several additional software suites are required to run this workflow:

-   [R2CCBC (v0.8.1)](https://bitbucket.org/ccbc/r2ccbc/). Commit used in this analysis: [e9dc0](https://bitbucket.org/ccbc/r2ccbc/commits/e9dc0db2d163ccf5e273f41425f47c82de5d77c6).
    -   R2CCBC can be installed using devtools: `devtools::install_bitbucket(repo = "ccbc/r2ccbc")`. To view all functions, type `help(package = R2CCBC)`.
-   [Variant Effect Predictor (release v99; hg19)](https://www.ensembl.org/info/docs/tools/vep/script/vep_download.html).
    -   The following command is used for the CPCT-02 cohort: `bash vep_parallel_v99.sh -v /pathTo/hartwig/DRXX/combinedData/ -e .purple.somatic.vcf.gz -o <output> -f /pathTo/genomes/hsapiens/hg19_HMF/Homo_sapiens.GRCh37.GATK.illumina.fasta`
-   [GISTIC2 (v2.0.23 with hg19.mat)](https://portals.broadinstitute.org/cgi-bin/cancer/publications/view/216/).
-   Additional R packages are installed via [pacman](https://cran.r-project.org/web/packages/pacman/index.html).
    -   All packages used are specified in each individual R script

## Running the workflow

After installation of `R2CCBC` on your local machine, the scripts can be performed in the order listed in the *R/* folder to generate all the respective data and corresponding figures; starting with `1.importAndPreProcessData.R` which imports the various input files and processes them into combined reports and various RData object for downstream analysis.

For ease, we generate symlinks of all required input data into a single folder (*combinedData/*). As part of the first step, the VCF files have to be further annotated with VEP v0.99, hg19.
