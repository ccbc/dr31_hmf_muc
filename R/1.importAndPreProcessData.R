### Import data of the DR31 CPCT-02 mUC cohort
#
# Script to pre-process data for the DR31-HMF-mUC cohort
# Date: Dec-2021
# Author Job van Riet, Harmen van de Werken and J. Alberto Nakauma-Gonzalez
# e-mail: j.nakaumagonzalez@erasmusmc.nl

# Variables ---------------------------------------------------------------
path.hmf <- "pathTo/DR31_HMF_data/"
path.samples <- paste0(path.hmf,"postHMF/combinedData/")
path.output <- paste0(path.hmf,"postHMF/RData/")

# load libraries
pacman::p_load('R2CCBC', 'ggplot2', 'ShatterSeek', 'InteractionSet', 'CHORD')

# load meta data
load(paste(path.output, "samples.meta.RData" ,sep ="/"))


# Import CPCT data --------------------------------------------------------

# Read all data.
cl <- BiocParallel::MulticoreParam(workers = 36, progressbar = TRUE, stop.on.error = TRUE)
BiocParallel::register(cl, default = TRUE)

data.Cohort <- BiocParallel::bplapply(samples.meta$sampleId, function(patient){
    importCPCT.allDataFromPatient(pathPatient = path.samples, patientIdentifier = as.character(patient), 
                                  importStructuralVariants = TRUE, 
                                  importSomaticVariants = TRUE,
                                  genome = 'hg19',
                                  minAD.ALT = 3, gnomAD_exome = 0.001, gnomAD_genome = 0.005,                      
                                  ncores = 1)
}, BPPARAM = cl)

BiocParallel::bpstop(cl)
names(data.Cohort) <- unlist(lapply(data.Cohort, function(x) unique(x$somaticVariants$sample)))
save(data.Cohort, file = file.path(path.output, 'data.DR31_mUCcohort.RData'))


# Perform GISTIC ----------------------------------------------------------

regions <- unlist(GenomicRanges::GenomicRangesList(lapply(data.Cohort, function(x) x$copyNumbers)))
regions <- R2CCBC::cleanSeqlevels(regions, excludeChr = NULL)

## later adapt R2CCBC?
tmpobj <- performGISTIC2(regions = regions, dryRun = T)
## type tmp file
tmpFile <- "/tmp/RtmpmdQPwT/750569ac5994.txt"
## mkdir output GISTIC
dir.create(Gout, recursive = TRUE)

# Run command in: /mnt/data/ccbc_environment/software/general/GISTIC2_2.0.23/
## since we don't use the assignment of the genes to the regions by GISTIC we don't have to use the same data sets as in VEP analysis
#(paste("cd /mnt/data/ccbc_environment/software/general/GISTIC2_2.0.23/; /mnt/data/ccbc_environment/software/general/GISTIC2_2.0.23/gistic2 -b /mnt/data/ccbc_environment/project/DR31_HMF/processed/DR31_HMF_data/v2/postHMF/results/GISTIC2/ -seg ",tmpFile," -refgene /mnt/data/ccbc_environment/software/general/GISTIC2_2.0.23/refgenefiles/hg19.UCSC.add_miR.140312.refgene.mat -genegistic 1 -gcm extreme -maxseg 4000 -broad 1 -brlen 0.98 -conf 0.95 -rx 0 -cap 3 -saveseg 0 -armpeel 1 -smallmem 0 -res 0.01 -ta 0.1 -td 0.1 -savedata 0 -savegene 1 -qvt 0.1"))
rm(tmpFile)
## /mnt/data/ccbc_environment/software/general/GISTIC2_2.0.23/gistic2 -b /mnt/data/ccbc_environment/project/DR31_HMF/processed/DR31_HMF_data/v2/postHMF/results/GISTIC2/ -seg /tmp/RtmpYxcZ2J/33c73be1d474.txt -refgene /mnt/data/ccbc_environment/software/general/GISTIC2_2.0.23/refgenefiles/hg19.UCSC.add_miR.140312.refgene.mat -genegistic 1 -gcm extreme -maxseg 4000 -broad 1 -brlen 0.98 -conf 0.95 -rx 0 -cap 3 -saveseg 0 -armpeel 1 -smallmem 0 -res 0.01 -ta 0.1 -td 0.1 -savedata 0 -savegene 1 -qvt 0.1

# Import previously run GISTIC.
#  Missing column names filled in: 'X129' [129] ignore last line has awkward ending
## ignore   In clusterProfiler::bitr(gisticAllGenesCN$`Gene ID`, fromType = "ENTREZID",  : 6.04% of input gene IDs are fail to map..
## all oncogenes and tumor suppressors do work
Gout <- paste0(path.hmf,"postHMF/results/GISTIC2/GISTIC_run15957/")
data.GISTIC <- R2CCBC::importGISTIC2(Gout)
save(data.GISTIC, file = file.path(path.output, 'data.GISTIC.RData'))


# Combine Reports ---------------------------------------------------------
combinedReports <- R2CCBC::generateCombinedOverviewOfCohort(idata.Cohort = data.Cohort, idata.GISTIC = data.GISTIC, pathdNdS = path.output, ncores = 5)
save(combinedReports, file = file.path(path.output, 'combinedReports.RData'))


# Mutational load ---------------------------------------------------------

data.Cohort.mutLoad <- R2CCBC::generateSomaticSummary(data.Cohort, ncores = 5)
data.Cohort.mutLoad <- merge(data.Cohort.mutLoad, samples.meta, by.x = 'sample', by.y='sampleId')
save(data.Cohort.mutLoad, file = file.path(path.output, 'data.Cohort.mutLoad.RData'))


# Mutational signatures ------------------------------------------------------------------------------

# Combine all mutations (SNV) of cohort.
mutSamples <- GenomicRanges::GRangesList(unlist(lapply(data.Cohort, function(x) x$somaticVariants[x$somaticVariants$set == 'snvs' & !is.na(x$somaticVariants$set),][,1:5])))
names(mutSamples) <- unlist(lapply(data.Cohort, function(x) unique(x$somaticVariants$sample)))

# Perform mutsig calling.
data.mutSigs <- R2CCBC::performMutSigMatch.Alexandrov(mutSamples, versionCOSMIC = 'v3', genome = 'hg19')
save(data.mutSigs, file = file.path(path.output, file = 'data.mutSigs.RData'))



# APOBEC Enrichment ----------------------------------------------------------------------------------
# Based on Roberts et. al., Nature Genetics, 2013 ######
require("BSgenome.Hsapiens.UCSC.hg19")

#get A, T, C, G content
params <- new("BSParams", X = Hsapiens, FUN = alphabetFrequency, exclude = c("M","Y","random","hap", "chrUn"))
ATCG_content <- bsapply(params)

# transform to data frame
ATCG_content <-  as.data.frame(do.call(rbind, ATCG_content))

# Calculate TCW motifs
params <- new("BSParams", X = Hsapiens, FUN = countPDict, exclude = c("M","Y","random","hap", "chrUn"))

# Get TCW (WGA) content
pdict_TCA <- PDict(DNAStringSet("TCA"))
TCW_content_TCA <- bsapply(params, pdict = pdict_TCA)

pdict_TCT <- PDict(DNAStringSet("TCT"))
TCW_content_TCT <- bsapply(params, pdict = pdict_TCT)

pdict_AGA <- PDict(DNAStringSet("AGA"))
TCW_content_AGA <- bsapply(params, pdict = pdict_AGA)

pdict_TGA <- PDict(DNAStringSet("TGA"))
TCW_content_TGA <- bsapply(params, pdict = pdict_TGA)

# Create data frame with all TCW (WGA) DNA context
TCW_content <-  as.data.frame(do.call(rbind, TCW_content_TCA))
TCW_content <-  cbind(TCW_content, as.data.frame(do.call(rbind, TCW_content_TCT)))
TCW_content <-  cbind(TCW_content, as.data.frame(do.call(rbind, TCW_content_AGA)))
TCW_content <-  cbind(TCW_content, as.data.frame(do.call(rbind, TCW_content_TGA)))
colnames(TCW_content) <- c("V1", "V2", "V3", "V4")

# Get total number of TCW motifs and C (G) bp
total_Context_CorG <- sum(ATCG_content$C + ATCG_content$G)
total_Context_TCW <- sum(TCW_content$V1 + TCW_content$V2 + TCW_content$V3 + TCW_content$V4)

# Count total number of C>T and C>G mutations and APOBEC mutations (TCW>TTW and TCW>TGW mutations) per sample
# Tri-nucleotide mutational context counts are stored in data.mutSigs$motifMatrix (data must be arranged)
TotalSNVsPerPatient_APOBEC <- SNVs_data_perSample %>%
  dplyr::select(sample, freq_Sample, APOBEC_Freq_Sample, C_T_Freq_Sample, C_G_Freq_Sample) %>%
  dplyr::group_by(sample) %>% dplyr::mutate(totalSNVs = sum(freq_Sample, na.rm = T),
                                            totalAPOBEC = sum(APOBEC_Freq_Sample, na.rm = T),
                                            total_CtoT_CtoG = sum(C_G_Freq_Sample, na.rm = T) + sum(C_T_Freq_Sample, na.rm = T)) %>%
  ungroup() %>% dplyr::select(sample, totalSNVs, totalAPOBEC, total_CtoT_CtoG) %>% dplyr::distinct(sample, .keep_all = TRUE)

# Calculate fold enrichment for APOBEC mutations
TotalSNVsPerPatient_APOBEC <- TotalSNVsPerPatient_APOBEC %>%
  dplyr::mutate(foldEnrichment = (totalAPOBEC * total_Context_CorG) / (total_CtoT_CtoG * total_Context_TCW))

# Initialize value for pValues calculation
TotalSNVsPerPatient_APOBEC$E_pValue <- 1

# calculate p-value for APOBEC enrichtment with Fisher's exact test
for (iPatient in c(1:nrow(TotalSNVsPerPatient_APOBEC))) {
  challenge.df = matrix(c(TotalSNVsPerPatient_APOBEC$totalAPOBEC[iPatient], total_Context_TCW,
                          TotalSNVsPerPatient_APOBEC$total_CtoT_CtoG[iPatient]-TotalSNVsPerPatient_APOBEC$totalAPOBEC[iPatient], total_Context_CorG - total_Context_TCW), nrow = 2)
  
  TotalSNVsPerPatient_APOBEC$E_pValue[iPatient] <- fisher.test(challenge.df, alternative = "greater")$p.value
}

# Adjust p-values with Benjamini-Hochberg method
TotalSNVsPerPatient_APOBEC$E_pAdj <- p.adjust(TotalSNVsPerPatient_APOBEC$E_pValue, method = "BH")

# Tumor is APOBEC enriched if p_Adj < 0.01
APOBECClassSample <- TotalSNVsPerPatient_APOBEC %>% dplyr::mutate(APOBEC_enrich = ifelse(E_pAdj < 0.01, "Yes", "No"))
APOBECClassSample$APOBEC_enrich <- factor(APOBECClassSample$APOBEC_enrich, levels = c("No", "Yes"))

# classify tumors in low, high or no APOBEC mutagenesis based on last result and fold enrichment
APOBECClassSample <- APOBECClassSample %>%
  dplyr::mutate(APOBEC_mutagenesis = ifelse(APOBEC_enrich == "Yes",
                                            ifelse(foldEnrichment > 2, "High", "Low"), "No"))
APOBECClassSample$APOBEC_mutagenesis <- factor(APOBECClassSample$APOBEC_mutagenesis, levels = c("No", "Low", "High"))

# Save complete APOBEC class data for use later in other 
save(APOBECClassSample, file = "/pathTo/RData/APOBECClassSample.RData")





# Run CHORD ------------------------------------------------------------------------------------
# Retrieve CHORD contexts.
outputCHORD.contexts <- pbapply::pblapply(samples.meta$sampleId, function(x){
    contexts <- CHORD::extractSigsChord(
                           vcf.snv = list.files(path.samples, pattern = paste0(x, '.*_post_processed_vep.vcf$'), full.names = T),
                           vcf.indel = list.files(path.samples, pattern = paste0(x, '.*_post_processed_vep.vcf$'), full.names = T),
                           vcf.sv = list.files(path.samples, pattern = paste0(x, '.*purple.sv.ann.vcf.gz$'), full.names = T),
                           sv.caller = 'gridss'
                       )

    rownames(contexts) <- x
    return(contexts)
}, cl = 40)

outputCHORD.contexts <- do.call(rbind, outputCHORD.contexts)

# Retrieve CHORD predictions and save results.
outputCHORD.results <- CHORD::chordPredict(outputCHORD.contexts, show.features = TRUE)
save(outputCHORD.results, file = file.path(path.output, 'CHORD.RData'))


# make MSI object based on SQL st ---------------------------------------------------------------
library(RMySQL)  
password <- function(prompt = "Password:"){
    cat(prompt)
    pass <- system('stty -echo && read ff && stty echo && echo $ff && ff=""',
                   intern=TRUE)
    cat('\n')
    invisible(pass)
}                                                                                                                      

# access database
mydb <- dbConnect(MySQL(), user='hvandewerken',
                  password=password(),
                  ### file in console
                 dbname='DR31_HMF',
                 host='127.0.0.1')

dbListTables(mydb)    

# apply filters
rs <- dbSendQuery(mydb,
                  'SELECT sampleId, count(*)/2859 as indelsPerMb, if(count(*)/2859 > 4, "MSI", "MSS" ) AS status 
  FROM DR31_HMF.somaticVariant
  WHERE filter = "PASS"
  AND type = "INDEL" AND repeatCount >= 4 AND length(alt) <= 50 AND length(ref) <= 50
  AND ((length(repeatSequence) BETWEEN 2 AND 4 ) OR
	   (length(repeatSequence) = 1 AND repeatCount >= 5))
  GROUP BY sampleId;')

# save MSI results
outputMSI.results<-  as_tibble(fetch(rs))
outputMSI.results<- dplyr::filter(outputMSI.results, sampleId %in% samples.meta$sampleId) %>% arrange(desc(indelsPerMb)) 
save(outputMSI.results, file = file.path(path.output, 'MSI.RData'))







# Estimate fusion genes with ARRIBA  (RNAseq) ---------------------------------------------------------------
#----------- STAR was ran with chimeric options -----------------
# get file names of all bam files (with chimeric reads) for RNAseq
listFileNames <- list.files(path = "/pathTo/BAM",
                            pattern = "\\.bam$", full.names = TRUE, recursive = TRUE)

# get name of patients
listFileNames <- data.frame(listFileNames)
colnames(listFileNames) <- "dirFile"
listFileNames <- listFileNames %>% dplyr::mutate(sampleId = gsub(".*BAM_chim/", "", dirFile)) %>%
  dplyr::mutate(sampleId = gsub("_Aligned.*", "", sampleId))

# Keep only included samples
listFileNames <- dplyr::filter(listFileNames, sampleId %in% samples.meta$sampleId)


# create a file with commands for all RNA samples
scriptToRun <- data.frame(cmd = character())
for (iPatient in c(1:nrow(listFileNames))) {
  scriptToRun <- rbind(scriptToRun, data.frame(cmd = paste0("./arriba -x ", listFileNames$dirFile[iPatient],
                                                            " -g /pathTo/*.gtf",
                                                            " -a /pathTo/hg19/*.fasta",
                                                            " -b database/blacklist_hg19_hs37d5_GRCh37_2018-11-04.tsv.gz",
                                                            " -P",
                                                            " -o /pathTo/output/ARRIBA/fusions/",  listFileNames$sampleId[iPatient], ".tsv")))
}

# create commands to run in command line
write.table(scriptToRun, file = "/pathTo/ARRIBA/runARRIBA_new.txt", sep = "\t",
            row.names = FALSE, col.names = FALSE, quote = FALSE)

# run the following command in command line
# cat $(cd pathTo/software/general/arriba_v1.2.0/ && pathTo/ARRIBA/runARRIBA.txt) | parallel -j 2






#-------------------------------- estimate promoter sites and mutations -----------------------------

# get gene annotations
gtf <- rtracklayer::import('/mnt/onco0002/repository/general/annotation/hg19/GENCODE/noChrPrefix_filtered_gencode.v33lift37.annotation.gtf.gz')
promoter_regions <- gtf
promoter_regions <- promoter_regions[promoter_regions$type == "UTR"]
seqlevelsStyle(promoter_regions) <- "UCSC"
promoter_regions_df <- GenomicRanges::as.data.frame(promoter_regions)

# get promoter region based on strand sense (the promoter is 1-1000 upstream the each gene
promoter_regions_df_neg <- promoter_regions_df %>% dplyr::filter(strand == "-") %>%
  arrange(desc(row_number())) %>% dplyr::distinct(gene_name, .keep_all = TRUE) %>%
  dplyr::mutate(start_promoter = end + 1, end_promoter = end + 1001)
promoter_regions_df <- promoter_regions_df %>% dplyr::filter(strand == "+") %>%
  dplyr::distinct(gene_name, .keep_all = TRUE) %>%
  dplyr::mutate(start_promoter = start - 1001, end_promoter = start - 1)
promoter_regions_df <- rbind(promoter_regions_df, promoter_regions_df_neg) %>% dplyr::arrange(seqnames, start)

# some promoters have two or more genes, only keep one
promoter_regions_df <- promoter_regions_df %>% dplyr::distinct(start, .keep_all = TRUE)


# fucntion to access variants from VCF
function_importCPCT.somaticVariants <- function(pathVCF, genome = 'hg19', passOnly = T, snpOnly = F, minAD.ALT = 3, gnomAD_exome = 0.001, gnomAD_genome = 0.001, keepAnnotation = T, ncores = 10){
  
  # Input validation --------------------------------------------------------
  
  checkmate::assertAccess(pathVCF, access = 'r')
  checkmate::assertCharacter(genome, pattern = 'hg19|hg38')
  checkmate::assertLogical(passOnly)
  checkmate::assertLogical(snpOnly)
  checkmate::assertNumber(minAD.ALT, null.ok = T)
  checkmate::assertDouble(gnomAD_exome)
  checkmate::assertDouble(gnomAD_genome)
  checkmate::assertLogical(keepAnnotation)
  checkmate::assertInt(ncores)
  
  
  # Read VCF ----------------------------------------------------------------
  
  futile.logger::flog.info('importCPCT.somaticVariants - Importing input: %s', pathVCF)
  
  # Clean seqlevels and add chromosome information.
  sample.VCF <- R2CCBC::cleanSeqlevels(VariantAnnotation::readVcf(file = pathVCF,  genome = genome), excludeChr = NULL)

  # Remove non-PASS variants. (PON and Strelka2 filtering)
  if(passOnly) sample.VCF <- sample.VCF[VariantAnnotation::fixed(sample.VCF)$FILTER == 'PASS']
  
  # Convert VCF to simplified GRanges ---------------------------------------	
  futile.logger::flog.debug('importCPCT.somaticVariants - Converting VCF to GRanges object.')
  
  # Retrieve variant locations.
  sample.GRanges <- DelayedArray::rowRanges(sample.VCF)
  
  # Remove empty fields.
  GenomicRanges::mcols(sample.GRanges)$paramRangeID <- NULL
  GenomicRanges::mcols(sample.GRanges)$QUAL <- NULL
  
  # Convert ALT to DNAString.
  GenomicRanges::mcols(sample.GRanges)$ALT <- sample.GRanges$ALT
  
  # Add the variant ID
  GenomicRanges::mcols(sample.GRanges)$ID <- base::rownames(sample.VCF)
  
  # Add the samplename.
  GenomicRanges::mcols(sample.GRanges)$sample <- VariantAnnotation::samples(VariantAnnotation::header(sample.VCF))
  
  # Add INFO fields ---------------------------------------------------------
  futile.logger::flog.debug('importCPCT.somaticVariants - Adding INFO fields')
  
  # Get INFO fields
  sample.INFO <- tibble::as_tibble(VariantAnnotation::info(sample.VCF))
  sample.INFO$ANN <- NULL
  sample.INFO <- do.call(cbind, lapply(sample.INFO, paste))
  sample.INFO[sample.INFO == '.'] <- NA
  sample.INFO[sample.INFO == 'NA'] <- NA
  sample.INFO[sample.INFO == 'character(0)'] <- NA
  sample.INFO <- sample.INFO[,!grepl('gnomADg|ClinVar|noChrPrefix_gencode', colnames(sample.INFO))]
  
  # Add INFO to GRanges.
  GenomicRanges::mcols(sample.GRanges) <- cbind(GenomicRanges::mcols(sample.GRanges), sample.INFO)
  
  # Add GENO fields ---------------------------------------------------------
  futile.logger::flog.debug('importCPCT.somaticVariants - Retrieving GENO fields')
  
  # Get the AD of REF and ALT
  sample.GENO <- data.frame(GT = as.character(VariantAnnotation::geno(sample.VCF)$GT))
  sample.GENO$AD.REF <- as.integer(apply(VariantAnnotation::geno(sample.VCF)$AD, 1, function(x) x[[1]][1]))
  sample.GENO$AD.ALT <- as.integer(apply(VariantAnnotation::geno(sample.VCF)$AD, 1, function(x) x[[1]][2]))
  sample.GENO$DP <- sample.GENO$AD.REF + sample.GENO$AD.ALT
  sample.GENO$VAF <- sample.GENO$AD.ALT / sample.GENO$DP
  
  labels.GENO <- c(GT = 'Proposed Strelka2 Genotype', 
                   AD.REF = 'Amount of REF reads.', 
                   AD.ALT = 'Amount of ALT reads.', 
                   DP = 'Amount of supporting REF and ALT reads.', 
                   VAF = 'Variant Allele frequency.')
  
  # Add INFO to GRanges.
  GenomicRanges::mcols(sample.GRanges) <- c(GenomicRanges::mcols(sample.GRanges), sample.GENO)
  
  # Add labels.
  GenomicRanges::mcols(sample.GRanges) <- Hmisc::upData(GenomicRanges::mcols(sample.GRanges), labels = labels.GENO, print = F)
  
  # Heuristic filtering -----------------------------------------------------
  futile.logger::flog.debug('importCPCT.somaticVariants - Performing heuristic filtering.')
  
  # Remove variants below minAD.ALT threshold.
  if(!is.null(minAD.ALT)) sample.GRanges <- sample.GRanges[sample.GRanges$AD.ALT >= minAD.ALT]
  
  # Return object -----------------------------------------------------------	
  
  return(sample.GRanges)
}


# load reference genome
ref_genome <- "BSgenome.Hsapiens.UCSC.hg19"
require(ref_genome, character.only = TRUE)


# create a data frame with all mutations that fall within the promoter regions
mutsInPromoter_data <- BiocGenerics::do.call(BiocGenerics::rbind, pbapply::pblapply(samples.meta$sampleId, function(patientIdentifier){
  pathVCF <- list.files(path.VEP, pattern = paste0(patientIdentifier, '_post_processed_vep.*[vcf|gz]$'), full.names = T)
  data.SomaticVariants <- function_importCPCT.somaticVariants(pathVCF = pathVCF, genome = 'hg19', ncores = 5, minAD.ALT = 3, gnomAD_exome = 0.001, gnomAD_genome = 0.005, keepAnnotation = F)
  
  overlapMuts <- IRanges::findOverlapPairs(data.SomaticVariants,# promoter_region_gr)
                                           GenomicRanges::GRanges(seqnames = promoter_regions_df$seqnames, IRanges(promoter_regions_df$start_promoter, promoter_regions_df$end_promoter),
                                                                  gene_name = promoter_regions_df$gene_name, gene_id = promoter_regions_df$gene_id, gene_type = promoter_regions_df$gene_type, transcript_type = promoter_regions_df$transcript_id))
  
  datPromoterMuts <- data.frame(sample = overlapMuts@first$sample,
                                chr = GenomeInfoDb::seqnames(overlapMuts@first),
                                start = IRanges::start(overlapMuts@first),
                                end = IRanges::end(overlapMuts@first),
                                ref = overlapMuts@first$REF,
                                alt = Biostrings::unstrsplit(overlapMuts@first$ALT),
                                VAF = overlapMuts@first$VAF,
                                overlappingGenes = overlapMuts@second$gene_name,
                                gene_id = overlapMuts@second$gene_id,
                                gene_type = overlapMuts@second$gene_type,
                                transcript_type = overlapMuts@second$transcript_type,
                                mutType = overlapMuts@first$set, 
                                mut_id = overlapMuts@first$ID,
                                #Get up(down)stream mutation and context
                                upstreamNuc = as.character(Biostrings::getSeq(BSgenome.Hsapiens.UCSC.hg19, GenomeInfoDb::seqnames(overlapMuts@first), BiocGenerics::start(overlapMuts@first) - 1, BiocGenerics::start(overlapMuts@first) - 1)),
                                downstreamNuc = as.character(Biostrings::getSeq(BSgenome.Hsapiens.UCSC.hg19, GenomeInfoDb::seqnames(overlapMuts@first), BiocGenerics::end(overlapMuts@first) + 1, BiocGenerics::end(overlapMuts@first) + 1)),
                                start_promoter = IRanges::start(overlapMuts@second),
                                end_promoter = IRanges::end(overlapMuts@second))
  
  return(datPromoterMuts)
  
}, cl = 6))


# some mutations are counted twice in the same sample, keep only one.
mutsInPromoter_data <- mutsInPromoter_data %>% dplyr::distinct(sample, start, .keep_all = TRUE)

# Find HotSpot muts
mutsInPromoter_data <- mutsInPromoter_data %>% dplyr::group_by(mut_id) %>% dplyr::add_count(name = "totalHotSpotMut") %>%
  dplyr::ungroup() %>% dplyr::mutate(isHotspot = ifelse(totalHotSpotMut>1, "Yes", "No"))

#Add gene promoter for which the mutation belongs to
mutsInPromoter_data <- mutsInPromoter_data %>% dplyr::left_join(dplyr::select(promoter_regions_df, start_promoter, gene_name),
                                                                by = "start_promoter")

# Get most mutated promoters
mutsInPromoter_data <- mutsInPromoter_data %>% dplyr::add_count(gene_name, name = "nMutsCohort")

# Count how many distinct patients have a mutation in a specific gene
mutsInPromoter_patient <- mutsInPromoter_data %>% dplyr::distinct(sample, gene_name) %>%
  dplyr::add_count(gene_name, name = "nPatientsWithMutInGene") %>% dplyr::distinct(gene_name, nPatientsWithMutInGene)
mutsInPromoter_data <- dplyr::left_join(mutsInPromoter_data, mutsInPromoter_patient, by = "gene_name")


# saver data
save(mutsInPromoter_data, "/pathTo/RData/mutsInPromoter_data.RData")

################
writeLines(capture.output(sessionInfo()), paste0(path.output,"/1.ImportData_sessionInfo.txt"))

